<?php
/**
 * @file
 * ethical_projects.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ethical_projects_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'openethical_projects';
  $page->task = 'page';
  $page->admin_title = 'Projects';
  $page->admin_description = '';
  $page->path = 'projects';
  $page->access = array(
    'type' => 'none',
    'settings' => NULL,
  );
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Projects',
    'name' => 'menu-secondary-menu',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_openethical_projects_panel_context';
  $handler->task = 'page';
  $handler->subtask = 'openethical_projects';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => 'projects-page',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'radix_burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'Projects';
  $display->uuid = '982eae68-a5ae-4408-a700-114fd582a3bb';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-0cc1073b-d8af-4b10-96e3-864ffc619332';
  $pane->panel = 'contentmain';
  $pane->type = 'views_panes';
  $pane->subtype = 'openethical_projects-panel_pane_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'view_mode' => 'teaser',
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
    'view_settings' => 'fields',
    'header_type' => 'none',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '0cc1073b-d8af-4b10-96e3-864ffc619332';
  $display->content['new-0cc1073b-d8af-4b10-96e3-864ffc619332'] = $pane;
  $display->panels['contentmain'][0] = 'new-0cc1073b-d8af-4b10-96e3-864ffc619332';
  $pane = new stdClass();
  $pane->pid = 'new-dd9bff17-f1db-4b4c-b5c1-7596cd82f0f7';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-hao1G1TZH1rjR2SCRP8pXgg9ChPnPHuO';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Status',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'dd9bff17-f1db-4b4c-b5c1-7596cd82f0f7';
  $display->content['new-dd9bff17-f1db-4b4c-b5c1-7596cd82f0f7'] = $pane;
  $display->panels['sidebar'][0] = 'new-dd9bff17-f1db-4b4c-b5c1-7596cd82f0f7';
  $pane = new stdClass();
  $pane->pid = 'new-cea5a44a-eef5-48bb-99c9-acafec65259c';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-tmZP9nQiw0fety1puoVIUoXbZb3eXQT8';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Category',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'cea5a44a-eef5-48bb-99c9-acafec65259c';
  $display->content['new-cea5a44a-eef5-48bb-99c9-acafec65259c'] = $pane;
  $display->panels['sidebar'][1] = 'new-cea5a44a-eef5-48bb-99c9-acafec65259c';
  $pane = new stdClass();
  $pane->pid = 'new-6372f865-57de-41b0-99d3-d86506f1da58';
  $pane->panel = 'sidebar';
  $pane->type = 'block';
  $pane->subtype = 'facetapi-D0B4Ufd2zBSc18uzkhKwm9IoeuGpXDO1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 1,
    'override_title_text' => 'Country',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '6372f865-57de-41b0-99d3-d86506f1da58';
  $display->content['new-6372f865-57de-41b0-99d3-d86506f1da58'] = $pane;
  $display->panels['sidebar'][2] = 'new-6372f865-57de-41b0-99d3-d86506f1da58';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['openethical_projects'] = $page;

  return $pages;

}
