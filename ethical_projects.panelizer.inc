<?php
/**
 * @file
 * ethical_projects.panelizer.inc
 */

/**
 * Implements hook_panelizer_defaults().
 */
function ethical_projects_panelizer_defaults() {
  $export = array();

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_project:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_project';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = 'project-page';
  $panelizer->css = '';
  $panelizer->pipeline = 'ipe';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'page_manager';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array(
    'panels_breadcrumbs_state' => 0,
    'panels_breadcrumbs_titles' => 'Projects',
    'panels_breadcrumbs_paths' => 'projects',
    'panels_breadcrumbs_home' => 1,
  );
  $display = new panels_display();
  $display->layout = 'burr_flipped';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
      'footer' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '1e5d18af-4422-4eb7-987d-4710c1accae1';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-ee770041-6c40-420c-8c21-d01dcbaef91a';
  $pane->panel = 'contentmain';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_project_meta_data';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ee770041-6c40-420c-8c21-d01dcbaef91a';
  $display->content['new-ee770041-6c40-420c-8c21-d01dcbaef91a'] = $pane;
  $display->panels['contentmain'][0] = 'new-ee770041-6c40-420c-8c21-d01dcbaef91a';
  $pane = new stdClass();
  $pane->pid = 'new-0f7d3fa8-9c29-419a-a769-e52bc64f4862';
  $pane->panel = 'contentmain';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_project_main_content';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '0f7d3fa8-9c29-419a-a769-e52bc64f4862';
  $display->content['new-0f7d3fa8-9c29-419a-a769-e52bc64f4862'] = $pane;
  $display->panels['contentmain'][1] = 'new-0f7d3fa8-9c29-419a-a769-e52bc64f4862';
  $pane = new stdClass();
  $pane->pid = 'new-08de936c-6477-4d50-8cba-693a986039f7';
  $pane->panel = 'contentmain';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_project_tags';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '08de936c-6477-4d50-8cba-693a986039f7';
  $display->content['new-08de936c-6477-4d50-8cba-693a986039f7'] = $pane;
  $display->panels['contentmain'][2] = 'new-08de936c-6477-4d50-8cba-693a986039f7';
  $pane = new stdClass();
  $pane->pid = 'new-f78788e0-d435-48a8-8300-aae883a64f3e';
  $pane->panel = 'contentmain';
  $pane->type = 'block';
  $pane->subtype = 'easy_social-easy_social_block_1';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 3;
  $pane->locks = array(
    'type' => 'none',
    'regions' => array(
      'contentmain' => 'contentmain',
      'footer' => 'footer',
    ),
  );
  $pane->uuid = 'f78788e0-d435-48a8-8300-aae883a64f3e';
  $display->content['new-f78788e0-d435-48a8-8300-aae883a64f3e'] = $pane;
  $display->panels['contentmain'][3] = 'new-f78788e0-d435-48a8-8300-aae883a64f3e';
  $pane = new stdClass();
  $pane->pid = 'new-ea6eae4d-1a9c-4920-8111-3eb91a0f0b0f';
  $pane->panel = 'footer';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_project_footer';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'ea6eae4d-1a9c-4920-8111-3eb91a0f0b0f';
  $display->content['new-ea6eae4d-1a9c-4920-8111-3eb91a0f0b0f'] = $pane;
  $display->panels['footer'][0] = 'new-ea6eae4d-1a9c-4920-8111-3eb91a0f0b0f';
  $pane = new stdClass();
  $pane->pid = 'new-5f3bc5a1-6b8e-4d4a-b4a0-d9088fccece5';
  $pane->panel = 'sidebar';
  $pane->type = 'panels_mini';
  $pane->subtype = 'oe_project_side_bar';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'context' => array(
      0 => 'panelizer',
    ),
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '5f3bc5a1-6b8e-4d4a-b4a0-d9088fccece5';
  $display->content['new-5f3bc5a1-6b8e-4d4a-b4a0-d9088fccece5'] = $pane;
  $display->panels['sidebar'][0] = 'new-5f3bc5a1-6b8e-4d4a-b4a0-d9088fccece5';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_project:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_project:default:default';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_project';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'default';
  $panelizer->css_class = 'project-default';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'radix_boxton';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'd6593f9f-b73f-4f22-b7dc-fe828f87e960';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-dcecfa5f-7820-46e3-9f3b-7654142db3c2';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_start_end_day';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'above',
    'formatter' => 'date_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'format_type' => 'long',
      'fromto' => 'both',
      'multiple_from' => '',
      'multiple_number' => '',
      'multiple_to' => '',
    ),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'dcecfa5f-7820-46e3-9f3b-7654142db3c2';
  $display->content['new-dcecfa5f-7820-46e3-9f3b-7654142db3c2'] = $pane;
  $display->panels['contentmain'][0] = 'new-dcecfa5f-7820-46e3-9f3b-7654142db3c2';
  $pane = new stdClass();
  $pane->pid = 'new-c6a82d28-901b-4bcc-a452-ad64846ca814';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'text_default',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = 'c6a82d28-901b-4bcc-a452-ad64846ca814';
  $display->content['new-c6a82d28-901b-4bcc-a452-ad64846ca814'] = $pane;
  $display->panels['contentmain'][1] = 'new-c6a82d28-901b-4bcc-a452-ad64846ca814';
  $pane = new stdClass();
  $pane->pid = 'new-45c8704b-f13c-4553-a3c4-5930929d0007';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => '',
      'image_link' => '',
    ),
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = '45c8704b-f13c-4553-a3c4-5930929d0007';
  $display->content['new-45c8704b-f13c-4553-a3c4-5930929d0007'] = $pane;
  $display->panels['contentmain'][2] = 'new-45c8704b-f13c-4553-a3c4-5930929d0007';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_project:default:default'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_project:default:featured';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_project';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'featured';
  $panelizer->css_class = 'project-featured';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'oe_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = 'a8ea8d5f-5218-44f7-bd2f-317147d5255b';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-c923ef40-86e2-434b-986e-e4d531d8fbd9';
  $pane->panel = 'contentmain';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 1,
    'markup' => 'h3',
    'id' => '',
    'class' => '',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'c923ef40-86e2-434b-986e-e4d531d8fbd9';
  $display->content['new-c923ef40-86e2-434b-986e-e4d531d8fbd9'] = $pane;
  $display->panels['contentmain'][0] = 'new-c923ef40-86e2-434b-986e-e4d531d8fbd9';
  $pane = new stdClass();
  $pane->pid = 'new-7bcab49a-3b7a-43bd-a6e1-1705b1880f91';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'smart_trim_format',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'trim_length' => '30',
      'trim_type' => 'words',
      'trim_suffix' => '...',
      'more_link' => '0',
      'more_text' => 'Read more',
      'summary_handler' => 'full',
      'trim_options' => array(
        'text' => 0,
      ),
    ),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '7bcab49a-3b7a-43bd-a6e1-1705b1880f91';
  $display->content['new-7bcab49a-3b7a-43bd-a6e1-1705b1880f91'] = $pane;
  $display->panels['contentmain'][1] = 'new-7bcab49a-3b7a-43bd-a6e1-1705b1880f91';
  $pane = new stdClass();
  $pane->pid = 'new-e29d305b-671f-4b14-8df0-6a8dbfd36bd6';
  $pane->panel = 'contentmain';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => 0,
    'override_title_text' => '',
    'build_mode' => 'teaser',
    'identifier' => '',
    'link' => 1,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 2;
  $pane->locks = array();
  $pane->uuid = 'e29d305b-671f-4b14-8df0-6a8dbfd36bd6';
  $display->content['new-e29d305b-671f-4b14-8df0-6a8dbfd36bd6'] = $pane;
  $display->panels['contentmain'][2] = 'new-e29d305b-671f-4b14-8df0-6a8dbfd36bd6';
  $pane = new stdClass();
  $pane->pid = 'new-3faaaf7d-8d5a-4605-9e63-45a7e9bf0482';
  $pane->panel = 'sidebar';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'medium',
      'image_link' => 'content',
    ),
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '3faaaf7d-8d5a-4605-9e63-45a7e9bf0482';
  $display->content['new-3faaaf7d-8d5a-4605-9e63-45a7e9bf0482'] = $pane;
  $display->panels['sidebar'][0] = 'new-3faaaf7d-8d5a-4605-9e63-45a7e9bf0482';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_project:default:featured'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_project:default:teaser';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_project';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'teaser';
  $panelizer->css_class = 'project-teaser';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'oe_basic';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'header' => NULL,
      'sidebar' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '4fdd09f2-372b-484d-b195-79e3955731bd';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-949f3966-8dc3-47e9-80bb-bc96aea57126';
  $pane->panel = 'contentmain';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:body';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'smart_trim_format',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'trim_length' => '30',
      'trim_type' => 'words',
      'trim_suffix' => '...',
      'more_link' => '0',
      'more_text' => 'Read more',
      'summary_handler' => 'trim',
      'trim_options' => array(
        'text' => 0,
      ),
    ),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '949f3966-8dc3-47e9-80bb-bc96aea57126';
  $display->content['new-949f3966-8dc3-47e9-80bb-bc96aea57126'] = $pane;
  $display->panels['contentmain'][0] = 'new-949f3966-8dc3-47e9-80bb-bc96aea57126';
  $pane = new stdClass();
  $pane->pid = 'new-5de10d9c-bcef-48f4-b3a4-4e73bbc7c208';
  $pane->panel = 'contentmain';
  $pane->type = 'node_links';
  $pane->subtype = 'node_links';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_title' => FALSE,
    'override_title_text' => '',
    'build_mode' => 'teaser',
    'identifier' => '',
    'link' => TRUE,
    'context' => 'panelizer',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array(
    'css_class' => 'link-wrapper',
  );
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '5de10d9c-bcef-48f4-b3a4-4e73bbc7c208';
  $display->content['new-5de10d9c-bcef-48f4-b3a4-4e73bbc7c208'] = $pane;
  $display->panels['contentmain'][1] = 'new-5de10d9c-bcef-48f4-b3a4-4e73bbc7c208';
  $pane = new stdClass();
  $pane->pid = 'new-28dedd0a-e69a-4054-b4cb-25a7499157d6';
  $pane->panel = 'header';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 1,
    'markup' => 'h3',
    'id' => '',
    'class' => '',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '28dedd0a-e69a-4054-b4cb-25a7499157d6';
  $display->content['new-28dedd0a-e69a-4054-b4cb-25a7499157d6'] = $pane;
  $display->panels['header'][0] = 'new-28dedd0a-e69a-4054-b4cb-25a7499157d6';
  $pane = new stdClass();
  $pane->pid = 'new-13eee571-0a7a-4594-bbae-a623bbe64ba8';
  $pane->panel = 'sidebar';
  $pane->type = 'entity_field';
  $pane->subtype = 'node:field_featured_image';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'label' => 'hidden',
    'formatter' => 'image',
    'delta_limit' => 0,
    'delta_offset' => '0',
    'delta_reversed' => FALSE,
    'formatter_settings' => array(
      'image_style' => 'thumbnail',
      'image_link' => 'content',
    ),
    'context' => 'panelizer',
    'override_title' => 1,
    'override_title_text' => '',
    'view_mode' => NULL,
    'widget_title' => NULL,
    'items_per_page' => NULL,
    'exposed' => array(
      'sort_by' => NULL,
      'sort_order' => NULL,
    ),
    'use_pager' => NULL,
    'pager_id' => NULL,
    'offset' => NULL,
    'link_to_view' => NULL,
    'more_link' => NULL,
    'path' => NULL,
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '13eee571-0a7a-4594-bbae-a623bbe64ba8';
  $display->content['new-13eee571-0a7a-4594-bbae-a623bbe64ba8'] = $pane;
  $display->panels['sidebar'][0] = 'new-13eee571-0a7a-4594-bbae-a623bbe64ba8';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_project:default:teaser'] = $panelizer;

  $panelizer = new stdClass();
  $panelizer->disabled = FALSE; /* Edit this to true to make a default panelizer disabled initially */
  $panelizer->api_version = 1;
  $panelizer->name = 'node:oe_project:default:tiny';
  $panelizer->title = 'Default';
  $panelizer->panelizer_type = 'node';
  $panelizer->panelizer_key = 'oe_project';
  $panelizer->no_blocks = FALSE;
  $panelizer->css_id = '';
  $panelizer->css = '';
  $panelizer->pipeline = 'standard';
  $panelizer->contexts = array();
  $panelizer->relationships = array();
  $panelizer->access = array();
  $panelizer->view_mode = 'tiny';
  $panelizer->css_class = '';
  $panelizer->title_element = 'H2';
  $panelizer->link_to_entity = TRUE;
  $panelizer->extra = array();
  $display = new panels_display();
  $display->layout = 'oe_empty';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
      'contentmain' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '%node:title';
  $display->uuid = '5a936252-c802-48c5-a291-021cee097b7b';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-7aa503fa-bdf0-4c6a-b11e-65c1092e408a';
  $pane->panel = 'contentmain';
  $pane->type = 'node_title';
  $pane->subtype = 'node_title';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'link' => 1,
    'markup' => 'none',
    'id' => '',
    'class' => '',
    'context' => 'panelizer',
    'override_title' => 0,
    'override_title_text' => '',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '7aa503fa-bdf0-4c6a-b11e-65c1092e408a';
  $display->content['new-7aa503fa-bdf0-4c6a-b11e-65c1092e408a'] = $pane;
  $display->panels['contentmain'][0] = 'new-7aa503fa-bdf0-4c6a-b11e-65c1092e408a';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $panelizer->display = $display;
  $export['node:oe_project:default:tiny'] = $panelizer;

  return $export;
}
