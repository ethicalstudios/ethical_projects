<?php
/**
 * @file
 * ethical_projects.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function ethical_projects_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:oe_project.
  $config['node:oe_project'] = array(
    'instance' => 'node:oe_project',
    'config' => array(),
  );

  return $config;
}
